# Дано целое число 1≤n≤40, необходимо вычислить n-е число Фибоначчи 
# (напомним, что F0=0, F1=1 и Fn=Fn−1+Fn−2 Fn=Fn−1+Fn−2 при n≥2 n≥2).
 
# Sample Input:
# 3
# Sample Output:
# 2

def fib(n):
    A = []
    A.insert(0, 1)
    A.insert(1, 1)
    if n <= 1 or n == 2:
        return A[0]
    else:
        for i in range(2, (n + 1)):
            A.append(A[i - 1] + A[i - 2])
        return (A[n - 1])

def main():
    n = int(input())
    print(fib(n))


if __name__ == "__main__":
    main()