# Первая строка входа содержит число операций 1≤n≤105. Каждая из последующих n
# строк задают операцию одного из следующих двух типов:
#
# 𝙸𝚗𝚜𝚎𝚛𝚝 𝚡, где 0≤x≤109 — целое число;
# 𝙴𝚡𝚝𝚛𝚊𝚌𝚝𝙼𝚊𝚡.
# Первая операция добавляет число x в очередь с приоритетами, 
# вторая — извлекает максимальное число и выводит его.

# Sample Input:

# 6
# Insert 200
# Insert 10
# ExtractMax
# Insert 5
# Insert 500
# ExtractMax

# Sample Output:

# 200
# 500


import heapq

tree = []
n = int(input())
i = 0
while i < n:
    x = input().split()
    if "Insert" in x:
        tree.append(int(x[1]))
        heapq._siftdown_max(tree, 0, len(tree)-1)
    if "ExtractMax" in x:
        print(int(heapq._heappop_max(tree)))
    i += 1