# Первая строка содержит количество предметов 1≤n≤103 и вместимость рюкзака 0≤W≤2⋅10^6.
# Каждая из следующих n строк задаёт стоимость 0≤ci≤2⋅106 и объём 0<wi≤2⋅10^6
# предмета (n, W, ci, wi — целые числа). 
# Выведите максимальную стоимость частей предметов (от каждого предмета можно 
# отделить любую часть, стоимость и объём при этом пропорционально уменьшатся), 
# помещающихся в данный рюкзак, с точностью не менее трёх знаков после запятой.

# Sample Input:
# 3 50
# 60 20
# 100 50
# 120 30
# Sample Output:
# 180.000


from collections import deque
from decimal import *

getcontext().prec = 5000

# Считываем условие
n, W = input().split()
assert (int(n) > 0)
assert (Decimal(W) >= Decimal(0))
n = int(n)
W = Decimal(W)
if n != 0:
    sample = []
    for i in range(n):
        sample.append(input().split())
        assert (Decimal(sample[i][0]) >= Decimal(0))
        assert (Decimal(sample[i][1]) > Decimal(0))
    # Сортируем по стоимости единицы объема
    # sampleSort = deque([sample[0]])

    try:
        sampleSort = sorted(sample, key=lambda st: Decimal(st[1]) / Decimal(st[0]), reverse = False)
        #print(sampleSort)
        cost = Decimal(0)  # Стоимость предметов в рюкзаке
        volume = Decimal(0)
        for j in range(n):

            if Decimal(sampleSort[j][1]) != Decimal(0):
                localCost = Decimal(sampleSort[j][0]) / Decimal(sampleSort[j][1])
            else:
                localCost = Decimal(0)

            if volume >= W:
                break
            else:
                localVolume = Decimal(0)
                while True:
                    if Decimal(localVolume) >= Decimal(sampleSort[j][1]): # закончились j-е предметы, выходим из цикла
                        #print('1')
                        break
                    if Decimal(localVolume) >= Decimal(W): # объем j-х предметов равен или больше объема рюкзака. Запонили на первой итереции и выхом.
                        #print('2')
                        break

                    if (volume + localVolume) >= W: #рюкзак наполнился в середине j-й итерации, выходим их цикла
                        #print('3')
                        break

                    else:
                        localVolume += Decimal(1) #продолжаем наполнять
                        #volume += localVolume
                        #print('4')
                volume += Decimal(localVolume)


                #print('объем загруженного на ', j, 'итерации ', volume)
                cost += Decimal(localVolume) * Decimal(localCost)


        print('%.3f' % cost)
        # print(cost)
    except ZeroDivisionError:
        print(Decimal(0.000))

