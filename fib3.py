# Поиск чисел фиббоначи через массив

# через массивы
def fib(n):
    A = []
    A.insert(0, 1)
    A.insert(1, 1)
    if n <= 1 or n == 2:
        return A[0]%10
    else:
        for i in range(2, (n + 1)):
            A.append(A[i - 1]%10 + A[i - 2]%10)
        return (A[n - 1]%10)

def main():
    n = int(input())
    print(fib(n))


if __name__ == "__main__":
    main()